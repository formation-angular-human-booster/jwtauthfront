import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {

  apiUrl = 'http://localhost:8000/api/planettes';
  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
    return this.http.get(this.apiUrl);
  }
}
