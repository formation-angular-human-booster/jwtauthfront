import { Component, OnInit } from '@angular/core';
import {PlanetService} from '../../services/planet.service';
import {Planet} from '../../models/planet';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  planettes: Planet[];
  constructor(private planetService: PlanetService) { }

  ngOnInit(): void {
    this.planetService.getAll().subscribe(data => {
      this.planettes = data['hydra:member'];
      console.log(this.planettes);
    });
  }

}
