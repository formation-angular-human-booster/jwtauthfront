import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {AuthGuardService} from './guards/auth.guard';
import {AdminComponent} from './components/admin/admin.component';
import {RoleGuardService} from './guards/role.guard.service';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
  {path: 'admin', component: AdminComponent,  canActivate: [RoleGuardService],
    data: {
      expectedRole: 'ROLE_ADMIN'
    } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
