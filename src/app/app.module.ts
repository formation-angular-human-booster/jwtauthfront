import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AuthGuardService} from './guards/auth.guard';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {AuthInterceptor, authInterceptorProviders} from './helpers/auth.interceptor';
import {RoleGuardService} from './guards/role.guard.service';

export function getToken() {
  return localStorage.getItem('auth-token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({ config: {
        tokenGetter: getToken
      }})
  ],
  providers: [
    AuthGuardService,
    authInterceptorProviders,
    RoleGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
